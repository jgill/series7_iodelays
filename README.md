# series7_iodelays

This project aims to measure the delays associated with ODELAY and IDELAY cells.
We determine an initialisation procedure for a 200 MHz reference clock with 
IDELAYCTRL cells and then investigate the effect of different tap settings on IODELAY cells.

To build the design:

$ cd syn
$ vivado -mode gui -source series7_iodelays.tcl




set_property LOC        H19         [get_ports sys_clk_p]
set_property LOC        G18         [get_ports sys_clk_n]
set_property IOSTANDARD DIFF_SSTL15 [get_ports sys_clk_p]
set_property IOSTANDARD DIFF_SSTL15 [get_ports sys_clk_n]
create_clock -name sysclk -period 5 [get_ports sys_clk_p]

set_property LOC        AM39     [get_ports {led_o[0]}]
set_property LOC        AN39     [get_ports {led_o[1]}]
set_property LOC        AR37     [get_ports {led_o[2]}]
set_property LOC        AT37     [get_ports {led_o[3]}]
set_property LOC        AR35     [get_ports {led_o[4]}]
set_property LOC        AP41     [get_ports {led_o[5]}]
set_property LOC        AP42     [get_ports {led_o[6]}]
set_property LOC        AU39     [get_ports {led_o[7]}]

set_property IOSTANDARD LVCMOS18 [get_ports {led_o[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_o[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_o[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_o[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_o[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_o[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_o[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_o[7]}]



set_property LOC        BA37     [get_ports fan_pwm_o]
set_property IOSTANDARD LVCMOS18 [get_ports fan_pwm_o]


set_property LOC        AR40     [get_ports {pbutton_i[0]}]; #nort
set_property LOC        AU38     [get_ports {pbutton_i[1]}]; #east
set_property LOC        AP40     [get_ports {pbutton_i[2]}]; #south
set_property LOC        AV39     [get_ports {pbutton_i[3]}]; #centre
set_property LOC        AW40     [get_ports {pbutton_i[4]}]; #west
set_property LOC        AV40     [get_ports {pbutton_i[5]}]; #cpureset

set_property IOSTANDARD LVCMOS18 [get_ports {pbutton_i[0]}]; #north    - nco_hb = lfsr
set_property IOSTANDARD LVCMOS18 [get_ports {pbutton_i[1]}]; #east     - cabledel + azimuthal position = lfsr
set_property IOSTANDARD LVCMOS18 [get_ports {pbutton_i[2]}]; #south    - odelay interpolation level 2
set_property IOSTANDARD LVCMOS18 [get_ports {pbutton_i[4]}]; #west     - delayctrl_reset
set_property IOSTANDARD LVCMOS18 [get_ports {pbutton_i[3]}]; #centre   - nco_reset
set_property IOSTANDARD LVCMOS18 [get_ports {pbutton_i[5]}]; #cpureset - ftw

# IOB packing
set_property IOB TRUE [get_ports io_o[*]]

set_property LOC        AK32     [get_ports io_o[0]]
set_property IOSTANDARD LVCMOS18 [get_ports io_o[0]]

set_property LOC        AJ32     [get_ports io_o[1]]
set_property IOSTANDARD LVCMOS18 [get_ports io_o[1]]


# Critical nets
#set_property LOC        SLICE_X51Y224 [get_cells {nco_edge_generation_i/d1_r_reg}]
#set_property LOC        SLICE_X51Y224 [get_cells {nco_edge_generation_i/d2_r_reg}]

# GPIO DIP switch
set_property LOC AV30 [get_ports gpio_i[0]]
set_property LOC AY33 [get_ports gpio_i[1]]
set_property LOC BA31 [get_ports gpio_i[2]]
set_property LOC BA32 [get_ports gpio_i[3]]
set_property LOC AW30 [get_ports gpio_i[4]]
set_property LOC AY30 [get_ports gpio_i[5]]
set_property LOC BA30 [get_ports gpio_i[6]]
set_property LOC BB31 [get_ports gpio_i[7]]

set_property IOSTANDARD LVCMOS18 [get_ports gpio_i[0]]
set_property IOSTANDARD LVCMOS18 [get_ports gpio_i[1]]
set_property IOSTANDARD LVCMOS18 [get_ports gpio_i[2]]
set_property IOSTANDARD LVCMOS18 [get_ports gpio_i[3]]
set_property IOSTANDARD LVCMOS18 [get_ports gpio_i[4]]
set_property IOSTANDARD LVCMOS18 [get_ports gpio_i[5]]
set_property IOSTANDARD LVCMOS18 [get_ports gpio_i[6]]
set_property IOSTANDARD LVCMOS18 [get_ports gpio_i[7]]

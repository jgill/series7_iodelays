library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity series7_iodelays is

   port (
        sys_clk_p     : in    std_logic;
        sys_clk_n     : in    std_logic;

        fan_pwm_o     : out   std_logic;
        pbutton_i     : in    std_logic_vector(5 downto 0); -- neswcr
        led_o         : out   std_logic_vector(7 downto 0);
        gpio_i        : in    std_logic_vector(7 downto 0);

        io_o          : out   std_logic_vector(1 downto 0) );

    attribute shreg_extract : string;
    attribute shreg_extract of series7_iodelays : entity is "no";

end entity;

architecture rtl of series7_iodelays is
  
   signal sys_clk          : std_logic;
   signal clk200_buf       : std_logic;   
   signal clk200           : std_logic;
   signal reset_clksys_cnt : unsigned(31 downto 0) := (others => '1');
   signal reset_clksys     : std_logic;
   signal clksys           : std_logic;

   signal clkfbout         : std_logic;
   signal clkfbout_buf     : std_logic;
   signal clkout0          : std_logic;
   signal reset_clk200     : std_logic;
   signal reset_clk200_r   : std_logic_vector(31 downto 0);

   signal locked           : std_logic;
   
   signal lfsr32           : std_logic_vector (31 downto 0);
   signal lfsr32_r         : std_logic_vector (31 downto 0) := X"abcd3210";
   signal pbutton_r        : std_logic_vector(5 downto 0);
   signal pbutton_h        : std_logic_vector(5 downto 0);
   signal pbutton_hh       : std_logic_vector(5 downto 0);
   signal pbutton_val      : std_logic_vector(5 downto 0);
   signal pbutton_val_r    : std_logic_vector(5 downto 0);
   signal pending          : std_logic_vector(5 downto 0);

   signal one              : std_logic := '1';

   signal rst_delayctrl    : std_logic;
   signal odelay_val       : std_logic_vector(4 downto 0);
   signal odelay_rst       : std_logic;
   signal delay_rdy        : std_logic;

   signal io_r             : std_logic_vector(1 downto 0);
   signal io_delay         : std_logic;

   signal pps_cnt          : unsigned(27 downto 0);   
   signal pps_cnt_r        : unsigned(27 downto 0) := X"0000000";
   signal pps              : std_logic;
   signal debounce_cnt     : std_logic_vector(31 downto 0);
   signal debounce_cnt_r   : std_logic_vector(31 downto 0);

   signal gpio_r           : std_logic_vector(7 downto 0);
   signal gpio_h           : std_logic_vector(7 downto 0);
                                                        

   attribute dont_touch : string;
   attribute dont_touch of io_r : signal is "true";
begin

   ibufds_i : ibufds
      port map (
         O  => sys_clk,
         IB => sys_clk_n,
         I  => sys_clk_p );


    mmcm_adv_inst : MMCME2_ADV
        generic map (
            BANDWIDTH            => "OPTIMIZED",
            CLKOUT4_CASCADE      => FALSE,
            COMPENSATION         => "ZHOLD",
            STARTUP_WAIT         => FALSE,
            DIVCLK_DIVIDE        => 1,
            CLKFBOUT_MULT_F      => 5.000,
            CLKFBOUT_PHASE       => 0.000,
            CLKFBOUT_USE_FINE_PS => FALSE,
            CLKOUT0_DIVIDE_F     => 5.000,
            CLKOUT0_PHASE        => 0.000,
            CLKOUT0_DUTY_CYCLE   => 0.500,
            CLKOUT0_USE_FINE_PS  => FALSE,
            CLKIN1_PERIOD        => 5.0,
            REF_JITTER1          => 0.010 )
        port map (
            CLKFBOUT            => clkfbout,
            CLKFBOUTB           => open,
            CLKOUT0             => clk200,
            CLKOUT0B            => open,
            CLKOUT1             => open,
            CLKOUT1B            => open,
            CLKOUT2             => open,
            CLKOUT2B            => open,
            CLKOUT3             => open,
            CLKOUT3B            => open,
            CLKOUT4             => open,
            CLKOUT5             => open,
            CLKOUT6             => open,

            CLKFBIN             => clkfbout_buf,
            CLKIN1              => sys_clk,
            CLKIN2              => '0',
            CLKINSEL            => '1',

            DADDR               => (others => '0'),
            DCLK                => '0',
            DEN                 => '0',
            DI                  => (others => '0'),
            DO                  => open,
            DRDY                => open,
            DWE                 => '0',
            
            PSCLK               => '0',
            PSEN                => '0',
            PSINCDEC            => '0',
            PSDONE              => open,

            LOCKED              => locked,
            CLKINSTOPPED        => open,
            CLKFBSTOPPED        => open,
            PWRDWN              => '0',
            RST                 => reset_clksys );
   
   bufg_fb : bufg
      port map (
         O => clkfbout_buf,
         I => clkfbout );
   
   bufg_sys : bufg
      port map (
         O => clksys,
         I => sys_clk );

   bufg_200 : bufg
      port map (
         O => clk200_buf,
         I => clk200 );

   process (clk200_buf, locked) is
   begin
      if (locked = '0') then
         reset_clk200_r <= (others => '1');
      else
         if rising_edge(clk200_buf) then
            reset_clk200_r <= '0' & reset_clk200_r(31 downto 1);
         end if;                              
      end if;
   end process;    
   
   reset_clk200 <= reset_clk200_r(0);
   reset_clksys <= reset_clksys_cnt(0);
   process (clksys) is
   begin
      if rising_edge(clksys) then
         reset_clksys_cnt <= '0' & reset_clksys_cnt(31 downto 1);
      end if;
   end process;

   lfsr32(0)           <= lfsr32_r(31) xor lfsr32_r(21) xor lfsr32_r(1) xor lfsr32_r(0);
   lfsr32(31 downto 1) <= lfsr32_r(30 downto 0);

   pending(5)     <= '1' when pbutton_hh(5) = '1' and pbutton_h(5) = '0' else '0';
   pending(4)     <= '1' when pbutton_hh(4) = '1' and pbutton_h(4) = '0' else '0';   
   pending(3)     <= '1' when pbutton_hh(3) = '1' and pbutton_h(3) = '0' else '0';
   pending(2)     <= '1' when pbutton_hh(2) = '1' and pbutton_h(2) = '0' else '0';
   pending(1)     <= '1' when pbutton_hh(1) = '1' and pbutton_h(1) = '0' else '0';
   pending(0)     <= '1' when pbutton_hh(0) = '1' and pbutton_h(0) = '0' else '0';

   debounce_cnt   <= X"ffffffff" when unsigned(pending) /= 0 else '0' & debounce_cnt_r(31 downto 1);
   
   pbutton_val(5) <= not pbutton_val_r(5) when pending(5) = '1' and debounce_cnt_r(0) = '0' else pbutton_val_r(5);
   pbutton_val(4) <= not pbutton_val_r(4) when pending(4) = '1' and debounce_cnt_r(0) = '0' else pbutton_val_r(4);
   pbutton_val(3) <= not pbutton_val_r(3) when pending(3) = '1' and debounce_cnt_r(0) = '0' else pbutton_val_r(3);
   pbutton_val(2) <= not pbutton_val_r(2) when pending(2) = '1' and debounce_cnt_r(0) = '0' else pbutton_val_r(2);
   pbutton_val(1) <= not pbutton_val_r(1) when pending(1) = '1' and debounce_cnt_r(0) = '0' else pbutton_val_r(1);
   pbutton_val(0) <= not pbutton_val_r(0) when pending(0) = '1' and debounce_cnt_r(0) = '0' else pbutton_val_r(0);
   
   process (clk200_buf) is
   begin
      if rising_edge(clk200_buf) then
         lfsr32_r        <= lfsr32;
         pbutton_r       <= pbutton_i;
         pbutton_h       <= pbutton_r;
         pbutton_hh      <= pbutton_h;
         pbutton_val_r   <= pbutton_val;
         pps_cnt_r       <= pps_cnt;
         debounce_cnt_r  <= debounce_cnt;
         gpio_r          <= gpio_i;
         gpio_h          <= gpio_r;
      end if;
   end process;

   rst_delayctrl <= pbutton_val_r(5);
   
   delayctrl_i  : idelayctrl
      port map (
         rdy    => delay_rdy,           -- 1-bit output: ready output
         refclk => clk200_buf,          -- 1-bit input: reference clock input
         rst    => rst_delayctrl );     -- 1-bit input: active high reset input

   odelay_val  <= pbutton_val_r(4 downto 0);   
   odelay_rst  <= not delay_rdy;
   
   io_odelaye2_0 : odelaye2
      generic map (
         cinvctrl_sel          => "FALSE",   -- enable dynamic clock inversion (false, true)
         delay_src             => "ODATAIN", -- delay input (odatain, clkin)
         high_performance_mode => "TRUE",    -- reduced jitter ("true"), reduced power ("false")
         odelay_type           => "VAR_LOAD",-- fixed, variable, var_load, var_load_pipe
         odelay_value          => 0,         -- output delay tap setting (0-31)
         pipe_sel              => "FALSE",   -- select pipelined mode, false, true
         refclk_frequency      => 200.0,     -- idelayctrl clock input frequency in mhz (190.0-210.0, 290.0-310.0).
         signal_pattern        => "DATA" )   -- data, clock input signal
      port map (
         cntvalueout => open,                -- output: counter value output
         dataout     => io_delay,            -- output: delayed data/clock output
         c           => clk200_buf,          -- input: clock input
         ce          => '0',                 -- input: active high enable increment/decrement input
         cinvctrl    => '0',                 -- input: dynamic clock inversion input
         clkin       => '0',                 -- input: clock delay input
         cntvaluein  => odelay_val,          -- input: counter value input
         inc         => '0',                 -- input: increment / decrement tap delay input
         ld          => gpio_h(0),           -- input: loads odelay_value tap delay in variable mode
         ldpipeen    => '0',                 -- input: enables the pipeline register to load data
         odatain     => io_r(0),             -- input: output delay data input
         regrst      => odelay_rst );        -- input: active-high reset tap-delay input

   process (clk200_buf) is
   begin
      if rising_edge (clk200_buf) then
         if (gpio_h(7) = '0') then
            io_r(0) <= lfsr32_r(15);
            io_r(1) <= lfsr32_r(15);
         else
            io_r(0) <= not io_r(0);
            io_r(1) <= not io_r(1);
         end if;
      end if;
   end process;

   -- io(1) emitted without delay.
   io_o(0) <= io_delay;
   io_o(1) <= io_r(1);
   
   -- outputs
   fan_pwm_o          <= one;
   led_o(5 downto 0)  <= pbutton_val_r;
   led_o(6)           <= odelay_rst;
   led_o(7)           <= one;

end architecture;


        
